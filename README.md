# Move_somewhere #

This is a simple package contains ROS server/client files, which allow users to move the TurtleBot with specific linear and angular velocity.  It can also be modified to navigate to specific position in the format of ROS PoseStamped of a known map.