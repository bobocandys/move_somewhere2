#!/usr/bin/env python

from __future__ import division
import rospy
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2

class Obstacle_Planner(object):
	#Computes and logs the time taken to process a point cloud.
	
	def __init__(self):
		self._num_calls = 0
		self.is_too_close = False

	def has_obstacle(self, data):
		#Computes the average point in a point cloud and saves timing info.
		
		points = pc2.read_points(data, field_names=['x', 'y', 'z'], skip_nans=True)
		threshold = 1.5
		front_range = 0.2
	    	num_points = 0
    		avg_z = 0
		last_x = 0
    	
	# Only adds up the depth values for objects that are within the range
    	for x, _, z in points:
		last_x = x
		# Adds up the depth value
		if x > -front_range and x < front_range:
			avg_z += z
			num_points += 1

	# Computes the average z(depth value) for front objects within the range
	if num_points > 0:
		avg_z /= num_points

	self._num_calls += 1

	rospy.loginfo('last x: {}, z: {}, time/point: {}'.format(last_x, avg_z, avg_z < threshold))
		
	if avg_z > threshold:
		# walkable distance
		self.is_too_close = False
		rospy.loginfo('Distance is >' + str(threshold))
	else:
		# too close, should not move
		self.is_too_close = True
		rospy.loginfo('DIstance is <' + str(threshold))
