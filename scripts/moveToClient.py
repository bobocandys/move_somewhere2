#! /usr/bin/env python

import rospy
import actionlib
import roslib  

roslib.load_manifest('actionlib_msgs')
roslib.load_manifest('move_somewhere')

from actionlib import *
from actionlib_msgs.msg import *
from move_somewhere.msg import *
from geometry_msgs.msg import *

def moveTo_client():
	client = actionlib.SimpleActionClient('move_to', moveToLocationAction)

	client.wait_for_server()
	
	# set the goal velocity and lasting time then 
	# pass them to the server(time should be int, else are floats)
	goal = moveToLocationGoal(time=0.5, linear_x=1.0, linear_y=0.0, linear_z=0.0, angular_x=0.0, angular_y=0.0, angular_z=0.0)


	client.send_goal(goal)

	client.wait_for_result()

	return client.get_result()

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('move_to_client')
        result = moveTo_client()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
