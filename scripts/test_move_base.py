#! /usr/bin/env python

import rospy
import actionlib
import roslib 
from Obstacle_Planner import *
from move_somewhere.msg import *
from geometry_msgs.msg import *

def main():
	rospy.init_node('move', anonymous=True)
	obstacle_planner = Obstacle_Planner()
	rospy.Subscriber('/pointcloud2_in', PointCloud2, obstacle_planner.has_obstacle,
	        queue_size=1)
	
	rospy.loginfo('thisismove' + str(obstacle_planner.close_flag))
	now = rospy.get_rostime()

	# dummy while loop: give some time for the subscriber to dectect objects
	while (rospy.get_rostime() - now < rospy.Duration(5)):
		rospy.loginfo('listening')

	client = actionlib.SimpleActionClient('move_to', moveToLocationAction)
	now = rospy.get_rostime()
	while obstacle_planner.close_flag and (rospy.get_rostime() - now < rospy.Duration(30)):
		client.wait_for_server()
		
		# set the goal velocity and lasting time, 
		# pass them to the server(time should be int, else are floats)
		goal = moveToLocationGoal(time=0.5, linear_x=0.1, linear_y=0.0, linear_z=0.0, angular_x=0.0, angular_y=0.0, angular_z=0.0)

		client.send_goal(goal)

		client.wait_for_result()
		
	rospy.loginfo('Done with moving')
	rospy.loginfo('Either running out of specific time or obstacle encountered')

if __name__ == '__main__':
	main()
