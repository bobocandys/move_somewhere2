#! /usr/bin/env python

import roslib
import rospy
import actionlib

roslib.load_manifest('actionlib_msgs')
roslib.load_manifest('move_somewhere')

from actionlib import *
from actionlib_msgs.msg import *
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from move_somewhere.msg import *

class moveToActionServer:
	
	def __init__(self):
		self.move_server = actionlib.SimpleActionServer('move_to', moveToLocationAction, self.executeCB)

	def executeCB(self, goal):

		# check if the goal is cancelled
		if self.move_server.is_preempt_requested():
			rospy.loginfo('%s: Preempted' % self._action_name)
			self.move_server.set_preempted()
			success = False

		# Sets the topic and the publisher
		topic_name = '/mobile_base/commands/velocity'
		pub_base = rospy.Publisher(topic_name, Twist, queue_size=10)

		# Move message
		move_msg = Twist()
		move_msg.linear = Vector3(goal.linear_x, goal.linear_y , goal.linear_z)
		move_msg.angular = Vector3(goal.angular_x, goal.angular_y, goal.angular_z)

		# get the current time
		now = rospy.get_rostime()
		# run for 10 seconds
		while (rospy.get_rostime() - now < rospy.Duration(goal.time)):
			pub_base.publish(move_msg)
 
		result = moveToLocationResult();
		result.simple_result = 1;
		self.move_server.set_succeeded(result)

if __name__ == '__main__':
	rospy.init_node('move_to')
	get_move_to_server = moveToActionServer()
	rospy.spin()
